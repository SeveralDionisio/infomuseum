#Info Museum Project
Please visit http://infomuseum.altervista.org/ for a general overview about this application.

Info Museum is a Java developed application made for the exam of Sistemi Multimediali from *University of Naples Federico II*.
 
It is made for Android operating system and can be installed only on Android Lollipop (5.0) and above.

Also, it require at least an Internet connection to download information about atworks from http://infomuseum.altervista.org/. 

##How to
Just download the app and start scanning qr codes that contains plain text from 0001 to 0009. Actually only these atwork are avaible.

#Components

##Activities
The following activities are used in this application for the user

| Activity                | Description
| ----------------------- | -----------
| MainActivity            | The entry point of the app where the use can scan a qr code or insert manually the code about an atwork 
| ActivityInserisciCodice | The activity where the user enter manually the code about the atwork
| DettagliOpera           | Show all information about the atwork stored into the database
| ApplicaFiltri        	  | Let the user to play with atwork's picture with some image filters

##Classes
The following classes are user to help activities to do their work

| Class                | Description
| ----------------------- | -----------
| ConvolutionMatrix       | Here is definied the main function and other data structure used by Filtri class in order to perform image filtering
| Filtri					     | A collection of function made for apply filters to a atwork image
| ParseJSON               | This class contains information about the atwork and parse the Json code retrive from the server