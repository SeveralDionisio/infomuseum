package com.example.marco.infomuseum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.FileOutputStream;

public class DettagliOpera extends AppCompatActivity {

    private String codice_opera;
    private String domain = "http://infomuseum.altervista.org/";

    ArrayAdapter<String> adatpter;
    ListView listView;
    TextView operaNome;
    ImageView operaImmagine;
    Bitmap btm_opera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettagli_opera);

        Intent intent = getIntent();    //Riceve l'intent con il codice
        this.codice_opera = intent.getStringExtra(MainActivity.CODICE_OPERA);   //Stringa codice opera

        this.operaNome = (TextView) findViewById(R.id.operaNome);
        this.adatpter = new ArrayAdapter<String>(this, R.layout.custom_textview);
        this.listView = (ListView) findViewById(R.id.listView);
        this.operaImmagine = (ImageView) findViewById(R.id.operaImmagine);

        ottieni_opera();
    }

    //Ottengo le informazioni sull'opera
    private void ottieni_opera() {
        final RequestQueue queue = Volley.newRequestQueue(this);
        final String url = this.domain+"get_opera_from_codice.php?codice="+this.codice_opera; //Realizza l'url

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("[null]") == false) {
                            ParseJson pj = new ParseJson(response);
                            pj.parseJson();
                            operaNome.setText(pj.nome);
                            adatpter.addAll(pj.autore, pj.museo, pj.descrizione);
                            listView.setAdapter(adatpter);
                            //Imposta l'immagine
                            ottieni_immagine(pj.codice, queue);
                        }
                        else {  //La pagina restituisce la stringa "[null]"
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Non va", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }

    private void ottieni_immagine(String codice_immagine, RequestQueue queue) {
        String url = this.domain+codice_immagine+".jpg";
        final ImageView immagine = (ImageView) findViewById(R.id.operaImmagine);

        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        btm_opera = bitmap; //Salva l'opera negli attributi della classe
                        immagine.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        immagine.setImageResource(android.R.drawable.stat_notify_error);
                    }
                });
        queue.add(request);
    }

    /* Invocata quando si clicca sull'immagine. Apre ApplicaFiltri inviando l'immagine stessa */
    public void imageview_operaimmagine_onclick(View view) {
        Intent intent = new Intent(this, ApplicaFiltri.class);
        Bitmap btm = this.btm_opera;

        /* Salva l'immagine nel disco */
        try {
            //Write file
            String filename = "bitmap.png";
            FileOutputStream stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
            btm.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            //btm.recycle();    //Con questo l'app crashava

            intent.putExtra("image", filename);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
