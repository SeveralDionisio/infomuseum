package com.example.marco.infomuseum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marco on 06/07/2016.
 */
public class ParseJson {
    public static String codice;
    public static String nome;
    public static String museo;
    public static String autore;
    public static String descrizione;

    private static String KEY_CODICE = "Codice";
    private static final String KEY_NOME = "Nome";
    private static final String KEY_MUSEO = "Museum";
    private static final String KEY_AUTORE = "Author";
    private static final String KEY_DESCRIZIONE = "Description";

    private String json;    //Riferimento alla stringa JSON
    private JSONArray array;

    public ParseJson(String j) { this.json = j; };

    protected void parseJson() {
        try {
            array = new JSONArray(this.json);
            JSONObject jo = array.getJSONObject(0);
            codice = jo.getString(KEY_CODICE);
            nome = jo.getString(KEY_NOME);
            museo = jo.getString(KEY_MUSEO);
            autore = jo.optString(KEY_AUTORE);
            descrizione = jo.optString(KEY_DESCRIZIONE);
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }
}
