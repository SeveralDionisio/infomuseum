package com.example.marco.infomuseum;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;

public class ApplicaFiltri extends AppCompatActivity {

    private ImageView image;
    private Bitmap btm;
    private Bitmap btm_temp;    //Quando aumento luminosita/contrasto viene modificata questa
    private SeekBar seekbar_luminosita;
    private SeekBar seekbar_contrasto;
    private TextView textview_seekbar_luminosita;
    private TextView textview_seekbar_contrasto;
    private EditText edittext_sharpening;
    private EditText edittext_smoothing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applica_filtri);

        Intent intent = getIntent();

        //Recupera l'immagine
        String filename = intent.getStringExtra("image");
        try {
            FileInputStream is = this.openFileInput(filename);
            btm = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /******************************************************/

        this.edittext_sharpening = (EditText) findViewById(R.id.edittext_sharpening);

        this.image = (ImageView) findViewById(R.id.imageView);
        this.btm_temp = btm.copy(Bitmap.Config.ARGB_8888, false);   //Inizializzo l'immagine temporanea
        Filtri.salvaBitmap(btm);    //Per il reset

        this.image.setImageBitmap(btm);

        this.textview_seekbar_luminosita = (TextView) findViewById(R.id.textview_seekbar_luminosita);
        this.textview_seekbar_contrasto = (TextView) findViewById(R.id.textview_seekbar_contrasto);

        this.seekbar_luminosita = (SeekBar) findViewById(R.id.seekbar_luminosita);
        seekbar_luminosita.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textview_seekbar_luminosita.setText(Integer.toString(i-127));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekbar_contrasto.setProgress(127); //Azzera l'altra progressbar
                textview_seekbar_contrasto.setText("0");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Applica la luminosità. Usa .getProgress()
                btm_temp = Filtri.doBrightness(btm, seekBar.getProgress()-127);
                image.setImageBitmap(btm_temp);
            }
        });

        this.seekbar_contrasto = (SeekBar) findViewById(R.id.seekbar_contrasto);
        seekbar_contrasto.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textview_seekbar_contrasto.setText(Integer.toString(i-127));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //E' necessario ripristinare la seekbar della luminosità
                seekbar_luminosita.setProgress(127);
                textview_seekbar_luminosita.setText("0");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Applica il contrasto. Usa .getProgress()
                btm_temp = Filtri.createContrast(btm, seekBar.getProgress()-127);
                image.setImageBitmap(btm_temp);
            }
        });
    }

    public void button_luminosita_onclick(View view) {  //Applica la luminosità
        this.seekbar_luminosita.setProgress(127);
        this.btm = btm_temp.copy(Bitmap.Config.ARGB_8888, false);
        image.setImageBitmap(btm);
        Toast.makeText(getApplicationContext(), "Luminosità applicata", Toast.LENGTH_LONG).show();
    }

    public void button_contrasto_onclick(View view) {   //Applica il contrasto
        this.seekbar_contrasto.setProgress(127);
        this.btm = btm_temp.copy(Bitmap.Config.ARGB_8888, false);
        image.setImageBitmap(btm);
        Toast.makeText(getApplicationContext(), "Contrasto applicato", Toast.LENGTH_LONG).show();
    }

    public void button_negativo_onclick(View view) {    //Applica il negativo
        reimposta_barre();

        Bitmap tmp = this.btm.copy(Bitmap.Config.ARGB_8888, false);
        this.btm = Filtri.doInvert(tmp);
        this.image.setImageBitmap(this.btm);
    }

    public void button_scalagrigi_onclick(View view) {  //Applica il negativo
        reimposta_barre();

        Bitmap tmp = this.btm.copy(Bitmap.Config.ARGB_8888, false);
        this.btm = Filtri.doGreyscale(tmp);
        this.image.setImageBitmap(this.btm);
    }

    public void button_gamma_onclick(View view) {   //Applica il negativo
        reimposta_barre();

        EditText edittext_r = (EditText) findViewById(R.id.edittext_r);
        EditText edittext_g = (EditText) findViewById(R.id.edittext_g);
        EditText edittext_b = (EditText) findViewById(R.id.edittext_b);

        float red = Float.valueOf(edittext_r.getText().toString());
        float green = Float.valueOf(edittext_g.getText().toString());
        float blue = Float.valueOf(edittext_b.getText().toString());

        Bitmap tmp = this.btm.copy(Bitmap.Config.ARGB_8888, false);
        this.btm = Filtri.doGamma(tmp, red, green, blue);
        this.image.setImageBitmap(this.btm);
    }

    public void button_gaussiano_onclick(View view) {
        reimposta_barre();

        Bitmap tmp = this.btm.copy(Bitmap.Config.ARGB_8888, false);
        this.btm = Filtri.applyGaussianBlur(tmp);
        this.image.setImageBitmap(this.btm);
    }

    public void button_sharpening_onclick(View view) {
        reimposta_barre();

        Bitmap tmp = this.btm.copy(Bitmap.Config.ARGB_8888, false);
        this.btm = Filtri.sharpen(tmp, Integer.parseInt(edittext_sharpening.getText().toString()));
        this.image.setImageBitmap(this.btm);
    }

    public void button_ripristina_onclick(View view) {  //Ripristina l'immagine
        this.btm = Filtri.original.copy(Bitmap.Config.ARGB_8888, false);    //Sovrascrive la vecchia immagine
        reimposta_barre();
        this.image.setImageBitmap(btm);
    }

    private void reimposta_barre() {
        seekbar_contrasto.setProgress(127); //Azzera l'altra progressbar
        textview_seekbar_contrasto.setText("0");
        seekbar_luminosita.setProgress(127);
        textview_seekbar_luminosita.setText("0");
    }
}
