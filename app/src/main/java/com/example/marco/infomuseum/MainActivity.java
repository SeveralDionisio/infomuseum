package com.example.marco.infomuseum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //variabili che ricevo dalla scansione
    static final public String CODICE_OPERA = "com.example.spart_000.projectathens.CODICE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //Avvia l'activity che legge il codice
    public void startScan(View v) {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        intent.putExtra("SAVE_HISTORY", false);
        intent.putExtra("SCAN_WIDTH", 420); //Si può anche usare 1024
        intent.putExtra("SCAN_HEIGHT", 420);
        intent.putExtra("PROMPT_MESSAGE", "");  //Elimina il messaggio durante lo scan
        //intent.putExtra("RESULT_DISPLAY_DURATION_MS", 100);
        startActivityForResult(intent, 0);
        /*Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");*/
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT"); //contenuto del QR Code
                //Richiamo la DettagliOpera activity
                Intent cicciopancetta = new Intent(this, DettagliOpera.class);
                cicciopancetta.putExtra(CODICE_OPERA, contents);    //Invio la stringa contenuta nel QR
                startActivity(cicciopancetta);
            } else
            if (resultCode == RESULT_CANCELED) {//Gestione dell'errore
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Nessun dato ricevuto!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    //In questa activity l'utente inserisce il codice manualmente
    public void sendMessage(View view){
        Intent intent = new Intent(this, ActivityInserisciCodice.class);
        startActivity(intent);
    }
}
