package com.example.marco.infomuseum;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityInserisciCodice extends AppCompatActivity {
    EditText edittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserisci_codice);

        //visualizza la roba buona
        this.edittext = (EditText) findViewById(R.id.editText);
        this.edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                sendMessage();  //Avvia l'activity
                return true;
            }
        });
    }

    //Richiama l'activity che si occupa di mostrare l'opera
    void sendMessage() {
        Intent cicciopancetta = new Intent(this, DettagliOpera.class);
        cicciopancetta.putExtra(MainActivity.CODICE_OPERA, this.edittext.getText().toString());
        startActivityForResult(cicciopancetta, 0);  //Questa Activity si aspetta un risultato
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 0) {
            Toast.makeText(getApplicationContext(), "Opera non trovata", Toast.LENGTH_LONG).show();
        }
    }
}
